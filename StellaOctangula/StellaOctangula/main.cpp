#include <iostream>


using namespace std;


class Engine
{
    private:
        int calculateFunction(int n)
        {
            return (n * ((2*n*n) - 1));
        }
    
        bool binarySearch(int low , int high , int toBeSearchedElement)
        {
            while (low <= high)
            {
                int mid     = (low + high)/2;
                int funcMid = calculateFunction(mid);
                if (funcMid > toBeSearchedElement)
                {
                    high = mid - 1;
                }
                else if (funcMid < toBeSearchedElement)
                {
                    low = mid + 1;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }
    
    public:
        bool checkIfTheNumberIsStellaOctangullaNumber(int n)
        {
            int index = 1;
            while (calculateFunction(index) < n)
            {
                index *= 2;
            }
            if (calculateFunction(index) == n)
            {
                return true;
            }
            return (binarySearch(index/2 , index , n));
        }
};

int main(int argc, const char * argv[])
{
    Engine e = Engine();
    if(e.checkIfTheNumberIsStellaOctangullaNumber(51))
    {
        cout<<"True"<<endl;
    }
    else
    {
        cout<<"False"<<endl;
    }
    return 0;
}
